//
//  myManager.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "MyManager.h"
#import "PostObject.h"
#import "AppDelegate.h"

@implementation MyManager{
    PostObject *selectedCell;
    int activeCellIndex;
    NSMutableArray *cellArray;
}

//@synthesize someProperty;  //replace with pertinent properties at own convenience

/*
    This singleton can be referenced anywhere by using:
 
    MyManager *sharedManager = [MyManager sharedManager];
 
    (given that the class is imported, obviously)
    Use the class for getters and setters instead of appdelegate
*/

#pragma mark Singleton Methods

+ (id)sharedManager {
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        //someProperty = @"Default Property Value";  //here goes the default value for a property
        
        AppDelegate *app = [[AppDelegate alloc] init];
        cellArray = [[NSMutableArray alloc]initWithArray:[app loadData]];
        
    }
    return self;
}

#pragma mark - Get/Set methods


- (void)setCellArray:(NSArray *)array{
    cellArray = [NSMutableArray arrayWithArray:array];
}

- (NSArray *)getCellArray{
    return cellArray;
}

- (void)setActiveCellIndex:(int)cellIndex{
    activeCellIndex = cellIndex;
}

- (int)getActiveCellIndex{
    return activeCellIndex;
}

- (void)savePost:(PostObject *)newPost{
    [cellArray replaceObjectAtIndex:activeCellIndex withObject:newPost];
}

- (void)addNewPost:(PostObject *)newPost{
    [cellArray addObject:newPost];
}

- (void)deletePost{
    [cellArray removeObjectAtIndex:activeCellIndex];
}

- (void)setSelectedCell:(PostObject *)cell{
    selectedCell = cell;
}

- (PostObject *)getSelectedCell{
    return selectedCell;
}

- (void)setNotification:(NSDate *)notTime withID:(NSString *)postID{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    
    NSString *formattedTime = [dateFormat stringFromDate:notTime];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = notTime;
    localNotification.alertBody = [NSString stringWithFormat:@"%@ - Alibi reminder", formattedTime];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:postID forKey:@"kTimerNameKey"];
    localNotification.userInfo = userInfo;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)cancelNotification:(NSString *)postID{
    for(UILocalNotification *notification in [[[UIApplication sharedApplication] scheduledLocalNotifications] copy]){
        NSDictionary *userInfo = notification.userInfo;
        if([postID isEqualToString:[userInfo objectForKey:@"kTimerNameKey"]]){
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}

- (void)dealloc {
    
}

@end
