//
//  ViewController.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-30.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"
#import "PostObject.h"
#import "MyManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController (){
    BOOL _bannerIsVisible;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collView;
@property (weak, nonatomic) IBOutlet UIView *adView;
@property NSArray *cellArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self setupViewState];
}

- (void)viewDidAppear:(BOOL)animated{
    //[self buttonState];
    [self.collView reloadData];
    [super viewDidAppear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    MyManager *sharedManager = [MyManager sharedManager];
    self.cellArray = [[NSArray alloc] init];
    self.cellArray = [sharedManager getCellArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View setup method(s)

- (void)setupViewState{
    bannerView = [[ADBannerView alloc]initWithFrame: CGRectMake(0, 0, 320, 50)];
    bannerView.delegate = self;
    [bannerView setBackgroundColor:[UIColor clearColor]];
    [self.adView addSubview: bannerView];
}

#pragma mark - New post button method(s)

- (IBAction)newPostButton:(id)sender{
    MyManager *sharedManager = [MyManager sharedManager];
    PostObject *post = [[PostObject alloc] init];
    [sharedManager setSelectedCell:post];
    [self performSegueWithIdentifier:@"openDetailView" sender:self];
}

#pragma mark - UICollectionView methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.cellArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    CustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellID" forIndexPath:indexPath];
    
    PostObject *co = self.cellArray[indexPath.row];
    cell.image.image = co.image;
    cell.label.text = co.label;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
        sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    //Skalar om cellerna för att bilderna ska få rätt aspect ratio
    PostObject *cell = self.cellArray[indexPath.row];
    
    float width = cell.image.size.width;
    float height = cell.image.size.height;
    float collViewHeight = self.collView.frame.size.height;
    
    float newHeight = collViewHeight - 20;
    float resizeRatio = height/newHeight;
    float newWidth = width/resizeRatio;
    
    if(newWidth == 0 || isnan(newWidth)){
        newWidth = 300;
    }
    
    return  CGSizeMake(newWidth,newHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    if (cell.isSelected){
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    }
    
    MyManager *sharedManager = [MyManager sharedManager];
    NSArray *arr = [sharedManager getCellArray];
    
    PostObject *co = arr[indexPath.row];
    [sharedManager setSelectedCell:co];
    
    int activeCell = (int)indexPath.row;
    [sharedManager setActiveCellIndex:activeCell];
    
    [self performSegueWithIdentifier:@"openDetailView" sender:self];
}


#pragma mark - iAd banner methods

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    //NSLog(@"Error loading");
    [bannerView setHidden:YES];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
    //NSLog(@"Ad loaded");
}

- (void)bannerViewWillLoadAd:(ADBannerView *)banner{
    //NSLog(@"Ad will load");
    [bannerView setHidden:NO];
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner{
    //NSLog(@"Ad did finish");
}

@end
