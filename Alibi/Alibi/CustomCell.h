//
//  CustomCell.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end

