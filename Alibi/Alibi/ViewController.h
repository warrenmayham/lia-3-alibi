//
//  ViewController.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-30.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface ViewController : UIViewController<ADBannerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>{
    ADBannerView *bannerView;
}

- (IBAction)newPostButton:(id)sender;

@end
