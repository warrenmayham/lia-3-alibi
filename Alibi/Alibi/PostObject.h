//
//  CellObject.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PostObject : NSObject <NSCoding>

@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *postID;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
