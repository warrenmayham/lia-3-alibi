//
//  myManager.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostObject.h"

@interface MyManager : NSObject

//@property (nonatomic, retain) NSArray *someProperty;

+ (id)sharedManager;

- (void)setCellArray:(NSArray *)array;
- (NSArray *)getCellArray;

- (void)setActiveCellIndex:(int)cellIndex;
- (int)getActiveCellIndex;

- (void)savePost:(PostObject *)newPost;
- (void)addNewPost:(PostObject *)newPost;
- (void)deletePost;

- (void)setSelectedCell:(PostObject *)cell;
- (PostObject *)getSelectedCell;

- (void)setNotification:(NSDate *)notTime withID:(NSString *)notName;
- (void)cancelNotification:(NSString *)notID;

@end
