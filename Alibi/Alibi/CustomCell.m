//
//  CustomCell.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "CustomCell.h"

/* This class is a custom made cell for collectionview. It's used to represent
 cells and transfer properties through its connected IBOutlets   */

@implementation CustomCell

- (id) init{
    if ([super init]){
        
    }
    return self;
}

@end
