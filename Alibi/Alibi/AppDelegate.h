//
//  AppDelegate.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-24.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSArray *)loadData;

@end

