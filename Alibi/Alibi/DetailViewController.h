//
//  DetailViewController.h
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-30.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface DetailViewController : UIViewController<ADBannerViewDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, FBSDKSharingDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    ADBannerView *bannerView;
}

- (IBAction)postOnFBButton:(id)sender;
- (IBAction)pickTimeButton:(id)sender;
- (IBAction)deletePostButton:(id)sender;

- (IBAction)dateOKButton:(id)sender;
- (IBAction)dateBackButton:(id)sender;

- (IBAction)postImage:(id)sender;
- (IBAction)takeNewPictureButton:(id)sender;
- (IBAction)pickFromAlbumButton:(id)sender;
- (IBAction)removeImageButton:(id)sender;
- (IBAction)backButton:(id)sender;
- (IBAction)postOnFB:(id)sender;

@end
