//
//  CellObject.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-12-02.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "PostObject.h"

/* This class is a custom object made to represent posts when stored in arrays
 and when transfering post information between methods/classes in the project */

@implementation PostObject

@synthesize label;
@synthesize image;
@synthesize text;
@synthesize date;

- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self != nil){
        self.label = [aDecoder decodeObjectForKey:@"label"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.date = [aDecoder decodeObjectForKey:@"date"];
        self.postID = [aDecoder decodeObjectForKey:@"postID"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:[self label] forKey:@"label"];
    [aCoder encodeObject:[self image] forKey:@"image"];
    [aCoder encodeObject:[self text] forKey:@"text"];
    [aCoder encodeObject:[self date] forKey:@"date"];
    [aCoder encodeObject:[self postID] forKey:@"postID"];
}

@end