//
//  DetailViewController.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-30.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "DetailViewController.h"
#import "PostObject.h"
#import "MyManager.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface DetailViewController (){
    BOOL _bannerIsVisible;
    BOOL _goesToCollView;  //används för att veta vilken vy man går till för att veta när posten ska sparas
    NSString *postIDString;
}

@property (weak, nonatomic) IBOutlet UIView *adView;
@property (weak, nonatomic) IBOutlet UIView *pickDateView;
@property (weak, nonatomic) IBOutlet UIView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *detailImageButton;
@property (weak, nonatomic) IBOutlet UIButton *takeNewPictureButton;
@property (weak, nonatomic) IBOutlet UIButton *pickFromAlbumButton;
@property (weak, nonatomic) IBOutlet UIButton *removeImageButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIButton *FBButton;

@property (nonatomic)UITextView *textView;
@property (weak, nonatomic)NSDate *postDate;

@end

@implementation DetailViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    [self setupViewState];
}

- (void)viewDidAppear:(BOOL)animated{
    BOOL navHidden = self.navigationController.navigationBarHidden;
    if(navHidden == YES){
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    }
    [self setupTextView];
    _goesToCollView = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    MyManager *sharedManager = [MyManager sharedManager];
    PostObject *post = [[PostObject alloc] init];
    post = [sharedManager getSelectedCell];
    
    BOOL postIsNil = NO;
    BOOL thereIsANewPost = NO;
    if(post.image == nil && post.text == nil && post.label == nil && post.date == nil){ //if newPostButton was clicked
        
        if(self.detailImageButton.currentImage == nil && [self.textView.text  isEqual: @""]){ //if no new post has been created
            postIsNil = YES;
        }else{
            thereIsANewPost = YES;
        }
    }
    
    if(_goesToCollView == YES && postIsNil == NO && thereIsANewPost == NO){ //if worked with existing post
        [self savePost:NO];
    }else if(_goesToCollView == YES && thereIsANewPost == YES){  // if a new post has been created
        [self savePost:YES];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View setup method(s)

- (void)setupViewState{
    //Laddar data till sidan från den valda posten
    MyManager *sharedManager = [MyManager sharedManager];
    PostObject *post = [sharedManager getSelectedCell];
    
    if(post.image.size.width > post.image.size.height){
        self.detailImageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.detailImageButton.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    
    [self.detailImageButton setImage:post.image forState:UIControlStateNormal];
    
    if(post.postID != nil){
        postIDString = post.postID;
    }
    if(post.date != nil){
        [self.datePicker setDate:post.date animated:NO];
        self.postDate = post.date;
    }
    if(self.detailImageButton.currentImage == nil){
        [self.detailImageButton setHidden:YES];
        [self hideButtons];
    }else{
        [self.detailImageButton setHidden:NO];
    }
    
    //state för iAds banner
    bannerView = [[ADBannerView alloc]initWithFrame: CGRectMake(0, 0, 320, 50)];
    bannerView.delegate = self;
    [bannerView setBackgroundColor:[UIColor clearColor]];
    [self.adView addSubview: bannerView];
    
    //Dynamisk storlek för imageView (bilden som hör till Post)
    float aspectRatio = 1.33;
    float newWidth = self.imageView.superview.frame.size.width * 0.6;
    self.imageViewWidth.constant = newWidth;
    self.imageViewHeight.constant = newWidth * aspectRatio;
    
    //GestureRecognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:tap];
    tap.delegate = self;
    
    //Set button state för FB-knappen
    if([FBSDKAccessToken currentAccessToken] == nil){
        [self.FBButton setImage:[UIImage imageNamed:@"FBLoggedOut.png"] forState:UIControlStateNormal];
        
    }else{
        [self.FBButton setImage:[UIImage imageNamed:@"FBLoggedIn.png"] forState:UIControlStateNormal];
    }
    
    //Justerar tidsramen för date picker
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setMonth:5];
    
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:[NSDate date] options:0];
    self.datePicker.maximumDate = maxDate;
    [comps setMonth:0];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:[NSDate date] options:0];
    self.datePicker.minimumDate = minDate;
}

- (void) setupTextView{
    [self.textView removeFromSuperview];
    
    float viewWidth = self.view.frame.size.width;
    float belowPostImage = self.imageView.frame.origin.y + self.imageView.frame.size.height + 10;
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(5, belowPostImage, viewWidth-10, 120)];
    
    [self.view addSubview:self.textView];
    [self.textView setDelegate:self];
    
    MyManager *sharedManager = [MyManager sharedManager];
    PostObject *cell = [sharedManager getSelectedCell];
    [self.textView setText:cell.text];
}

#pragma mark - Save post method

- (void)savePost:(BOOL)isNewPost{  //Här sammanställs och sparas den skapade posten
    MyManager *sharedManager = [MyManager sharedManager];
    PostObject *post = [[PostObject alloc] init];
    bool postIsEmpty = YES;
    
    if(self.detailImageButton.currentImage != nil){
        post.image = self.detailImageButton.currentImage;
        postIsEmpty = NO;
    }
    if(self.postDate != nil){
        post.date = self.postDate;   /*Detta stycke hämtar och formaterar datum*/
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yy/MM/dd':' HH:mm"];
        
        post.label = [dateFormat stringFromDate:post.date];
        postIsEmpty = NO;
    }
    if(self.textView.text != nil){
        post.text = self.textView.text;
        postIsEmpty = NO;
    }
    if(postIDString == nil && post.postID == nil){
        post.postID = [[NSUUID UUID] UUIDString];
    }else{
        post.postID = postIDString;
    }
    
    if(postIsEmpty == NO && isNewPost == NO){   //uppdaterar redan existerande post
        [sharedManager savePost:post];
        
        if(post.date != nil){  //uppdaterar local notification
            [sharedManager cancelNotification:post.postID];
            [sharedManager setNotification:post.date withID:post.postID];
        }
    }else if(postIsEmpty == NO && isNewPost == YES){  //sparar ny post
        [sharedManager addNewPost:post];
        
        if(post.date != nil){  //Skapar local notification
            [sharedManager setNotification:post.date withID:post.postID];
        }
    }
}

#pragma mark - Keyboard sensitivity methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    return YES;
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)keyboardDidShow:(NSNotification *)notification{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = keyboardFrameBeginRect.size.height;
    float screenHeight = self.view.frame.size.height;
    float kbUpperYCoord = screenHeight - keyboardHeight;
    float tvHeight = self.textView.frame.size.height;
    float tvWidth = self.textView.frame.size.width;
    float tvXpos = self.textView.frame.origin.x;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.textView.frame = CGRectMake(tvXpos, kbUpperYCoord-tvHeight, tvWidth, tvHeight);
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardDidHide:(NSNotification *)notification{

    float belowPostImage = self.imageView.frame.origin.y + self.imageView.frame.size.height + 10;
    [UIView animateWithDuration:0.3f animations:^{
        self.textView.frame = CGRectMake(5, belowPostImage, self.view.frame.size.width-10, 120);
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - select social media (only FB atm) button methods

- (IBAction)postOnFBButton:(id)sender{
    if([FBSDKAccessToken currentAccessToken] == nil){  //Om man inte är inloggad så loggas man in
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login
         logInWithReadPermissions: @[@"public_profile"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
                 [self.FBButton setImage:[UIImage imageNamed:@"FBLoggedIn.png"] forState:UIControlStateNormal];
             }
         }];
    }else if([FBSDKAccessToken currentAccessToken] != nil){
        [[FBSDKLoginManager new] logOut];
        [self.FBButton setImage:[UIImage imageNamed:@"FBLoggedOut.png"] forState:UIControlStateNormal];
    }
}

#pragma mark - share fb method

- (IBAction)postOnFB:(id)sender{
    if(self.detailImageButton.currentImage != nil){
        
        if([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]){
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];  //Skapar den enskilda posten som ska upp på FB
            photo.caption = self.textView.text;
            photo.image = self.detailImageButton.currentImage;
            photo.userGenerated = YES;
            
            FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];  //Skapar en content som kan innehålla flera grejer
            content.photos = @[photo];
            
            [FBSDKShareAPI shareWithContent:content delegate:self];  //Lägger upp det på FB
            
        }else{  //om appen inte redan har tillåtelse att publicera på facebook
            
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                
                FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];  //Skapar den enskilda posten som ska upp på FB
                photo.caption = self.textView.text;
                photo.image = self.detailImageButton.currentImage;
                photo.userGenerated = YES;
                
                FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];  //Skapar en content som kan innehålla flera grejer
                content.photos = @[photo];
                
                [FBSDKShareAPI shareWithContent:content delegate:self];  //Lägger upp det på FB
            }];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"You need to include an image." message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        }];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - FBSDKSharingDelegate methods
- (void)sharer:	(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    NSLog(@"Completed sharing");
}
- (void)sharer:	(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    NSLog(@"Sharing failed with error: %@", error);
}
- (void) sharerDidCancel:(id<FBSDKSharing>)sharer{
    NSLog(@"Sharing was canceled");
}


#pragma mark - Delete post method

- (IBAction)deletePostButton:(id)sender{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Confirm"
                                          message:@"Delete this post?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    UIAlertAction *deleteAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Delete", @"Delete action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   MyManager *sharedManager = [MyManager sharedManager];
                                   PostObject *post = [sharedManager getSelectedCell];
                                   
                                   if(post.image == nil && post.text == nil && post.label == nil && post.date == nil){
                                       
                                   }else{
                                       [sharedManager deletePost];
                                   }
                                   _goesToCollView = NO;
                                   [sharedManager cancelNotification:postIDString];
                                   [self performSegueWithIdentifier:@"openCollView" sender:nil];
                               }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Pick time methods

- (IBAction)pickTimeButton:(id)sender{
    [_pickDateView setHidden:NO];
    [self.view endEditing:YES];
    [self.textView setHidden:YES];
    float belowPostImage = self.imageView.frame.origin.y + self.imageView.frame.size.height + 10;
    self.textView.frame = CGRectMake(5, belowPostImage, self.view.frame.size.width-10, 120);
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (IBAction)dateOKButton:(id)sender{
    [_pickDateView setHidden:YES];
    [self.textView setHidden:NO];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.postDate = self.datePicker.date;
}

- (IBAction)dateBackButton:(id)sender{
    [_pickDateView setHidden:YES];
    [self.textView setHidden:NO];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}


#pragma mark - Detail Image Button methods

- (IBAction)postImage:(id)sender{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self hideButtons];
}

- (IBAction)removeImageButton:(id)sender{
    [_detailImageButton setImage:nil forState:UIControlStateNormal];
    [self hideButtons];
}

- (IBAction)backButton:(id)sender{
    [self hideButtons];
}

#pragma mark - Camera/Album Button Methods

- (IBAction)takeNewPictureButton:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){ //om kamera är tillgänglig
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        picker.allowsEditing = false;
        
        _goesToCollView = NO;
        
        [self presentViewController:picker animated:true completion:nil];
    }
}

- (IBAction)pickFromAlbumButton:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        picker.allowsEditing = true;
        
        _goesToCollView = NO;
        
        [self presentViewController:picker animated:true completion:nil];
    }
}

#pragma mark - Image picker (camera/album) delegate method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if(image.size.width > image.size.height){
        self.detailImageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.detailImageButton.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    [self.detailImageButton setImage:image forState:UIControlStateNormal];
    [self hideButtons];
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - Hide buttons method

- (void)hideButtons{ //Här flippas via button-click hidden-states för knapparna i bild-vyn i detailview
    
    if(_detailImageButton.isHidden == YES && _detailImageButton.currentImage != nil){ //om bilden är inladdad och gömd
        
        [_detailImageButton setHidden:NO];
        [_takeNewPictureButton setHidden:YES];
        [_pickFromAlbumButton setHidden:YES];
        [_removeImageButton setHidden:YES];
        [_backButton setHidden:YES];
    }
    else if(_detailImageButton.isHidden == YES && _detailImageButton.currentImage == nil){ //om bilden är gömd och inte har bild
        
        [_takeNewPictureButton setHidden:NO];
        [_pickFromAlbumButton setHidden:NO];
        [_removeImageButton setHidden:YES];
        [_backButton setHidden:YES];
    }
    else if(_detailImageButton.isHidden == NO){  //om bilden inte är gömd
        
        [_detailImageButton setHidden:YES];
        [_takeNewPictureButton setHidden:NO];
        [_pickFromAlbumButton setHidden:NO];
        
        if(_detailImageButton.currentImage != nil){ //dessa knappar ska bara visas om det finns en inladdad bild
            [_removeImageButton setHidden:NO];
            [_backButton setHidden:NO];
        }
    }
}


#pragma mark - iAd banner methods

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    //NSLog(@"Error loading");
    [bannerView setHidden:YES];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
    //NSLog(@"Ad loaded");
}

- (void)bannerViewWillLoadAd:(ADBannerView *)banner{
    //NSLog(@"Ad will load");
    [bannerView setHidden:NO];
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner{
    //NSLog(@"Ad did finish");
    
}

@end
























