//
//  AppDelegate.m
//  Alibi
//
//  Created by Martin Wilhelmsson on 2015-11-24.
//  Copyright © 2015 Martin Wilhelmsson. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "PostObject.h"
#import "AppDelegate.h"
#import "MyManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    [self setupNavBar];
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    //Nedprioriterat men kvarlämnat. Här hanteras vad som ska hända om notifikation kommer när appen är aktiv.
    //Ett meddelande via uialertcontroller vore annars lämpligt 
}


#pragma mark - Navigation bar setup

- (void)setupNavBar{
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:255.0/255.0 green:131.0/255.0 blue:73.0/255.0 alpha:1.0]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                    [UIFont fontWithName:@"AvenirNext-Medium" size:20.0], NSFontAttributeName, nil]
                                                forState:UIControlStateNormal];
    
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                    [UIFont fontWithName:@"AvenirNext-Medium" size:20.0], NSFontAttributeName, nil]];
    
}


#pragma mark - Facebook methods

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


#pragma mark - Save/Load methods

- (void)saveData:(NSArray *)array {  //Sparar data och assignar en key
    
    int i = 0;
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    while(i < array.count){
        PostObject *post = array[i];
        NSMutableArray *postArr = [[NSMutableArray alloc] init];  //varje post-egenskap sparas som nsdata i en array i dataArray
        
        if(post.image != nil){
            NSData *imgData = UIImageJPEGRepresentation(post.image, 1.0);
            [postArr insertObject:imgData atIndex:0];
        }else{
            [postArr insertObject:@"0" atIndex:0]; //för att allt ska hamna rätt när man laddar får index inte vara helt tom
        }
        if(post.label != nil){
            NSData *lblData = [post.label dataUsingEncoding:NSUTF8StringEncoding];
            [postArr insertObject:lblData atIndex:1];
        }else{
            [postArr insertObject:@"0" atIndex:1];
        }
        if(post.text != nil){
            NSData *textData = [post.text dataUsingEncoding:NSUTF8StringEncoding];
            [postArr insertObject:textData atIndex:2];
        }else{
            [postArr insertObject:@"0" atIndex:2];
        }
        if(post.date != nil){
            NSData *dateData = [NSKeyedArchiver archivedDataWithRootObject:post.date];
            [postArr insertObject:dateData atIndex:3];
        }else{
            [postArr insertObject:@"0" atIndex:3];
        }
        if(post.postID != nil){
            NSData *postIDData = [post.postID dataUsingEncoding:NSUTF8StringEncoding];
            [postArr insertObject:postIDData atIndex:4];
        }else{
            [postArr insertObject:@"0" atIndex:4];
        }
        
        [dataArray addObject:postArr];
        i++;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *savedPosts = [NSKeyedArchiver archivedDataWithRootObject:dataArray];
    [defaults setObject:savedPosts forKey:@"savedPosts"];
    
    [defaults synchronize];
}

- (NSArray *)loadData{  //Ladda data från nsuserdefaults med angiven key
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *loadedPosts = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"savedPosts"]];
    NSMutableArray *processedPosts = [[NSMutableArray alloc] init];
    [defaults synchronize];
    
    int i = 0;
    while(i < loadedPosts.count){
        PostObject *post = [[PostObject alloc] init];
        NSMutableArray *postArray = [[NSMutableArray alloc] init];
        postArray = loadedPosts[i];
        
        if(postArray[0] != nil && ![postArray[0] isEqual: @"0"]){
            UIImage *convImg = [UIImage imageWithData:postArray[0]];
            post.image = convImg;
        }
        if(postArray[1] != nil && ![postArray[1] isEqual: @"0"]){
            NSString *convLabel = [[NSString alloc] initWithData:postArray[1] encoding:NSUTF8StringEncoding];
            post.label = convLabel;
        }
        if(postArray[2] != nil && ![postArray[2] isEqual: @"0"]){
            NSString *convText = [[NSString alloc] initWithData:postArray[2] encoding:NSUTF8StringEncoding];
            post.text = convText;
        }
        if(postArray[3] != nil && ![postArray[3] isEqual: @"0"]){
            NSDate *convDate = [NSKeyedUnarchiver unarchiveObjectWithData:postArray[3]];
            post.date = convDate;
        }
        if(postArray[4] != nil && ![postArray[4] isEqual: @"0"]){
            NSString *convPostID = [[NSString alloc] initWithData:postArray[4] encoding:NSUTF8StringEncoding];
            post.postID = convPostID;
        }
        [processedPosts addObject:post];
        i++;
    }
    
    return processedPosts;
}


#pragma mark - AppDelegate methods

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    MyManager *sharedManager = [MyManager sharedManager];
    [self saveData:[sharedManager getCellArray]];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    MyManager *sharedManager = [MyManager sharedManager];
    [self saveData:[sharedManager getCellArray]];
}
/*
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}*/

@end
