# README #

This project repository is for an iPhone application written in Objective C. It was my final project during my education and it was designed to develop my programming skills rather than to have any form of profitability value. The purpose of the app is for the user to prepare posts for social media (Facebook) and post them via the app. I chose this project because it allowed me get used to a variety of useful components: the internal camera, the photo album, iAd, UICollectionView, UILocalNotification and the Facebook SDK via Cocoapods.

Created by Martin Wilhelmsson